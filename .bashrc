#!/bin/bash
myscripts=`cd`
if [ -d "/Users" ]; then
	export PATH="$PATH:$HOME/.rvm/bin:$myscripts" # Add RVM to PATH for scripting
	export PATH="$PATH:$myscripts/mac"
	userhome=/Users/justin
	desktop=$userhome/Desktop
	documents=$userhome/Documents
	downloads=$userhome/Downloads

	alias ossiue='ssh jmiles@os.cs.siue.edu'
	alias homesiue='ssh jmiles@home.cs.siue.edu'
	alias stantzsiue='ssh jmiles@stantz.cs.siue.edu'
	alias la='ls -a'
	alias ll='ls -la'
	alias quit='exit'

	bashrc=/users/justin/.bashrc
	gitjustin=http://github.com/justmiles94

	alias ds='cd $desktop'
	alias dc='cd $documents'
	jmsiue=jmiles@os.cs.siue.edu
	alias cdls='cd ..; ls'
	alias dw='cd $downloads'
	alias er='open $1'
	
	go(){
		cd $(dirname `which $1`)
	}
	emacs(){
		emacs -nw $*
	}

    	echo Unix based system.
	pwd
	hostname
	thisos=unix
elif [ -d "C:/Users" ]; then
	set /M ds=%USERPROFILE%\desktop
	set /M dc=%USERPROFILE%\documents
	set /M dw=%USERPROFILE%\downloads
	set /M 
	echo Windows System
	cd
	echo %USERNAME%
	thisos=windows
else
	echo unrecognized OS structure!
fi

pwd 
