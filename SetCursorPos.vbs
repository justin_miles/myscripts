Public Class SetCursorPos
  Private Declare Function SetCursorPos Lib "user32" Alias "SetCursorPos" _
    (ByVal x As Integer, ByVal y As Integer) As Integer

  Public Sub MoveTheCursor(ByVal xPos As Integer, ByVal yPos As Integer)
    SetCursorPos(xPos, yPos)
  End Sub
End Class